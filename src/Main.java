import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String cipherAlgorithm = "RSA";
        String signatureAlgorithm = "SHA256withRSA";

        System.out.println("Generando Claves...");
        GeneradorClaves.generate(cipherAlgorithm);

        System.out.println("Escriba el mensaje a cifrar: ");
        String message = sc.nextLine();

        try {
            //Cifrar y Guardar Mensaje
            CifradoYFirma.saveCipheredMessage(CifradoYFirma.cipherMessage(cipherAlgorithm,CifradoYFirma.reedFile(),message));
            System.out.println("Mensaje cifrado y guardado");

            //Crear y Guardar Firma Digital
            CifradoYFirma.saveSignature(CifradoYFirma.generateSiganture(signatureAlgorithm,CifradoYFirma.reedFile(),message,cipherAlgorithm));
            System.out.println("Firma creada y guardada");

            //Descrifar y Verificar Firma
            DescifrarYFirma.verifySignature(signatureAlgorithm,cipherAlgorithm,DescifrarYFirma.reedCipheredMessageFromFile());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
