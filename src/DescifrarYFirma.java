import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class DescifrarYFirma {
    private String cipherAlgorithm;
    private String signatureAlgorithm;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private byte[] cipheredMessage;
    private Signature signature;

    public DescifrarYFirma(String cipherAlgorithm, String signatureAlgorithm, PrivateKey privateKey, PublicKey publicKey, byte[] cipheredMessage, Signature signature) {
        this.cipherAlgorithm = cipherAlgorithm;
        this.signatureAlgorithm = signatureAlgorithm;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.cipheredMessage = cipheredMessage;
        this.signature = signature;
    }

    public String getCipherAlgorithm() {
        return cipherAlgorithm;
    }

    public void setCipherAlgorithm(String cipherAlgorithm) {
        this.cipherAlgorithm = cipherAlgorithm;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public byte[] getCipheredMessage() {
        return cipheredMessage;
    }

    public void setCipheredMessage(byte[] cipheredMessage) {
        this.cipheredMessage = cipheredMessage;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    private static byte[] reedFile(String fileName) throws IOException {
        FileInputStream fis = new FileInputStream(fileName);
        byte[] cipheredMessageBytes = new byte[fis.available()];
        fis.read(cipheredMessageBytes);
        fis.close();
        return cipheredMessageBytes;
    }

    public static byte[] reedCipheredMessageFromFile() {
        try {
            byte[] cipheredMessage = reedFile("cipheredMessage");
            return cipheredMessage;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static PublicKey reedPublicKeyFromFile(String cipherAlgorithm) {
        try {
            byte[] publicKeyBytes = reedFile("publicKey");
            PublicKey publicKey = KeyFactory.getInstance(cipherAlgorithm).generatePublic(new X509EncodedKeySpec(publicKeyBytes));
            return publicKey;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] decipherMessage(String cipherAlgorithm, PublicKey publicKey, byte[] cipheredMessage) {
        try {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] decipheredMessage = cipher.doFinal(cipheredMessage);
            System.out.println("Mensaje descifrado: " + new String(decipheredMessage));
            return decipheredMessage;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        } catch (BadPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] reedSignatureFromFile() {
        try {
            byte[] byteSignature = reedFile("firma");
            return byteSignature;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void verifySignature(String signatureAlgorithm, String cipherAlgorithm, byte[] cipheredMessage) {
        try {
            Signature signature = Signature.getInstance(signatureAlgorithm);
            signature.initVerify(reedPublicKeyFromFile(cipherAlgorithm));
            signature.update(decipherMessage(cipherAlgorithm, reedPublicKeyFromFile(cipherAlgorithm), cipheredMessage));
            boolean verification = signature.verify(reedSignatureFromFile());
            if (verification) {
                System.out.println("La firma es válida");
            } else {
                System.out.println("La firma no es válida");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }
}
