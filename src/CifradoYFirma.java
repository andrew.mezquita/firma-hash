import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

public class CifradoYFirma {
    private String cipherAlgorithm;
    private String signatureAlgorithm;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private String message;
    private byte[] cipheredMessage;
    private Signature signature;

    public CifradoYFirma(String cipherAlgorithm, String signatureAlgorithm, PrivateKey privateKey, PublicKey publicKey, String message, byte[] cipheredMessage, Signature signature) {
        this.cipherAlgorithm = cipherAlgorithm;
        this.signatureAlgorithm = signatureAlgorithm;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.message = message;
        this.cipheredMessage = cipheredMessage;
        this.signature = signature;
    }

    public String getCipherAlgorithm() {
        return cipherAlgorithm;
    }

    public void setCipherAlgorithm(String cipherAlgorithm) {
        this.cipherAlgorithm = cipherAlgorithm;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public byte[] getCipheredMessage() {
        return cipheredMessage;
    }

    public void setCipheredMessage(byte[] cipheredMessage) {
        this.cipheredMessage = cipheredMessage;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    private static byte[] reedMessage(String message) {
        byte[] bytesMessage = message.getBytes();
        return bytesMessage;
    }
    public static byte[] reedFile() throws IOException {
        FileInputStream fis = new FileInputStream("privateKey");
        byte[] privateKeyBytes = new byte[fis.available()];
        fis.read(privateKeyBytes);
        fis.close();
        return privateKeyBytes;
    }

    private static PrivateKey reedPrivateKey(byte[] privateKeyBytes, String cipherAlgorithm) {
        try {
            privateKeyBytes = reedFile();
            PrivateKey privateKey = KeyFactory.getInstance(cipherAlgorithm).generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
            return privateKey;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] cipherMessage(String cipherAlgorithm, byte[] privateKeyBytes, String message){
        try {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            cipher.init(Cipher.ENCRYPT_MODE,reedPrivateKey(privateKeyBytes, cipherAlgorithm));
            byte[] cipheredMessage = cipher.doFinal(reedMessage(message));
            return cipheredMessage;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        } catch (BadPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void saveCipheredMessage(byte[] cipheredMessage) {
        try (FileOutputStream fos = new FileOutputStream("cipheredMessage")) {
            fos.write(cipheredMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static byte[] generateSiganture(String signatureAlgorithm, byte[] privateKeyBytes, String message, String cipherAlgorithm){
        try {
            Signature signature = Signature.getInstance(signatureAlgorithm);
            signature.initSign(reedPrivateKey(privateKeyBytes, cipherAlgorithm));
            signature.update(reedMessage(message));
            byte[] bytesSignature = signature.sign();
            return bytesSignature;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }

    public static void saveSignature( byte[] bytesSignature) {
        try (FileOutputStream fos = new FileOutputStream("firma")) {
            fos.write(bytesSignature);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
